# Flectra Community / project-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[project_task_report](project_task_report/) | 2.0.1.0.0| Basic report for project tasks.


